/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation'
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
class Home extends Component<Props> {

  render() {
    return (
      <View style={styles.container}>
        {/*<Text style={styles.welcome}>Welcome to React Native!</Text>*/}
        {/*<Text style={styles.instructions}>To get started, edit App.js</Text>*/}
        {/*<Text style={styles.instructions}>{instructions}</Text>*/}

        <View style={styles.halfScreenContainer}>
          <TouchableOpacity style={{backgroundColor: '#ff3336', width: '50%', justifyContent: 'center'}}
                            onPress={ () => {
                              this.props.navigation.navigate('DetailRed')
                            }}>
            <Text style={styles.colourCode}>R</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: '#77ff74', width: '50%', justifyContent: 'center'}}
                            onPress={ () => {
                              this.props.navigation.navigate('DetailGreen')
                            }}>
            <Text style={styles.colourCode}>G</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.halfScreenContainer}>
          <TouchableOpacity style={{backgroundColor: '#3a47ff', width: '50%', justifyContent: 'center'}}
                            onPress={ () => {
                              this.props.navigation.navigate('DetailBlue')
                            }}>
            <Text style={styles.colourCode}>B</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: '#fff871', width: '50%', justifyContent: 'center'}}
                            onPress={ () => {
                              this.props.navigation.navigate('DetailYellow')
                            }}>
            <Text style={styles.colourCode}>Y</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

class DetailRed extends Component<Props> {
  render() {
    return (
      <View style={{
        flex: 1,
        // width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff3336',
      }}>

        <Text style={styles.colourCode}>R</Text>
        <Text style={styles.instructions}>Ini adalah halaman RED </Text>

      </View>
    );
  }
}

class DetailGreen extends Component<Props> {
  render() {
    return (
      <View style={{
        flex: 1,
        // width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#77ff74',
      }}>

        <Text style={styles.colourCode}>G</Text>
        <Text style={styles.instructions}>Ini adalah halaman GREEN </Text>

      </View>
    );
  }
}

class DetailBlue extends Component<Props> {
  render() {
    return (
      <View style={{
        flex: 1,
        // width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3a47ff',
      }}>

        <Text style={styles.colourCode}>B</Text>
        <Text style={styles.instructions}>Ini adalah halaman BLUE </Text>

      </View>
    );
  }
}

class DetailYellow extends Component<Props> {
  render() {
    return (
      <View style={{
        flex: 1,
        // width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff871',
      }}>

        <Text style={styles.colourCode}>Y</Text>
        <Text style={styles.instructions}>Ini adalah halaman YELLOW </Text>

      </View>
    );
  }
}

const AppNavigator = createStackNavigator({
  HomeScreen: Home,
  DetailRed: DetailRed,
  DetailBlue: DetailBlue,
  DetailGreen: DetailGreen,
  DetailYellow: DetailYellow,
});

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  // View
  container: {
    flex: 1,
    // width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  halfScreenContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#000',
  },

  // Text
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginTop: 8,
    fontSize: 17,
  },
  colourCode: {
    fontSize: 60,
    color: '#000',
    textAlign: 'center',
    fontWeight: 'bold',
  }
});
