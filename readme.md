# Repo for Final Exam
Subject: Web App Development
Universitas Prasetiya Mulya

## Pre-requisites
1. React navigation => npm install --save react-navigation
2. Gesture handler => npm install --save react-native-gesture-handler
3. Link gesture handler lib => react-native link react-native-gesture-handler
4. All set!

Developed by: Hubert Tatra
All rights reserved